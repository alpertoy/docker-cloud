## 1. How to run step by step

### 1.1 Create Network

`docker network create springcloud`

### 1.2 Pull OpenJDK12

Pull the image
`docker pull openjdk:12`

### 1.3 Run Config Server

Pull the image
`docker pull alpertoy/config-server`

Run the image
`docker run -p 8888:8888 --name config-server --network springcloud alpertoy/config-server:v1`

## 1.4 Run Eureka Server

Pull the image
`docker pull alpertoy/service-eureka-server`

Run the image
`docker run -p 8761:8761 --name service-eureka-server --network springcloud alpertoy/service-eureka-server:v1`

## 1.5 Run MySQL (for Products Service)

Pull the image
`docker pull mysql:8`

Run the image
`docker run -p 3306:3306 --name microservices-mysql8 --network springcloud -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=db_springboot_cloud -d mysql:8`

Check logs
`docker logs -f microservices-mysql8`

## 1.6 Run PostgreSQL (for Users Service)

Pull the image
`docker pull postgres:12-alpine`

Run the image
`docker run -p 5432:5432 --name microservices-postgres12 --network springcloud -e POSTGRES_PASSWORD=password -e POSTGRES_DB=db_springboot_cloud -d postgres:12-alpine`

Check logs
`docker logs -f microservices-postgres12`

## 1.7 Run Products Service

Pull the image
`docker pull alpertoy/service-products`

Run the image
`docker run -P --network springcloud alpertoy/service-products:v1`

## 1.8 Run Items Service

Pull the image
`docker pull alpertoy/service-items`

Run the image
`docker run -P --network springcloud alpertoy/service-items:v1`

## 1.9 Run Users Service

Pull the image
`docker pull alpertoy/service-users`

Run the image
`docker run -P --network springcloud alpertoy/service-users:v1`

## 1.10 Run Oauth Service

Pull the image
`docker pull alpertoy/service-oauth`

Run the image
`docker run -p 9100:9100 --network springcloud alpertoy/service-oauth:v1`

## 1.11 Run Zuul Server

Pull the image
`docker pull alpertoy/service-zuul-server`

Run the image
`docker run -p 8090:8090 --network springcloud alpertoy/service-zuul-server:v1`

## 1.12 Run RabbitMQ

Pull the image
`docker pull rabbitmq:3.8-management-alpine`

Run the image
`docker run -p 15672:15672 -p 5672:5672 --name microservices-rabbitmq38 --network springcloud -d rabbitmq:3.8-management-alpine`

Check logs
`docker logs -f microservices-rabbitmq38`

RabbitMQ Management UI
`http://localhost:15672`

Credentials:
Username: guest
Password: guest

## 2. How to test step by step

I recommend you to use Postman for testing API

### 2.1 Fetch all the data

Fetch list of Users
`localhost:8090/api/users/users`

Fetch list of Products
`localhost:8090/api/products/list`

Fetch list of Items
`localhost:8090/api/items/list`

### 2.2 Authentication

User (can only read data)
Username: user
Password: 12345

Admin (can create, read, update and delete data)
Username: admin
Password: 12345

Oauth authentication
Client ID: oauth
Client Password: 12345

Copy your token via POST request from
`localhost:8090/api/security/oauth/token`

3 maps for x-www-form-encoded

key: username
value: user or admin whatever your choice

key: password
value: 12345

key: grant_type
value: password

### 2.3 Fetch data by ID

After being authenticated as user or admin successfully,

Fetch Users by ID
`localhost:8090/api/users/users/id`

Fetch Products by ID
`localhost:8090/api/products/id`

Fetch Items by ID
`localhost:8090/api/items/id`

### 2.4 Delete data by ID

After being authenticated as admin successfully,

Delete Users by ID
`localhost:8090/api/users/users/id`

Delete Products by ID
`localhost:8090/api/products/delete/id`

Delete Items by ID
`localhost:8090/api/items/delete/id`

### 2.5 Update data by ID

After being authenticated as admin successfully,

Update Users by ID
`localhost:8090/api/users/users/id`

Update Products by ID
`localhost:8090/api/products/edit/id`

Update Items by ID
`localhost:8090/api/items/edit/id`

### 2.6 Create data

You have to be authenticated as admin

Create Users
`localhost:8090/api/users/users/`

```javascript
{
  "username": "Newuser",
  "password": "12345",
  "enabled": true,
  "name": "newuser",
  "surname": "new",
  "email": "newuser@user.com",
  "roles": [
    {
      "id":1, "name": "ROLE_USER"
    },
    {
      "id":2, "name": "ROLE_ADMIN"
    }
  ]
}
```

Create Products
`localhost:8090/api/products/create`

```javascript
{
    "name": "New Product",
    "price": 1500.0,
    "createAt": "2020-08-10"
}
```

### 2.7 Run Zipkin for tracing steps above

Pull the image
`docker pull alpertoy/zipkin-server`

Run the image
`docker run -p 9411:9411 --name zipkin-server --network springcloud -e RABBIT_ADDRESSES=microservices-rabbitmq38:5672 -e STORAGE_TYPE=mysql -e MYSQL_USER=zipkin -e MYSQL_PASS=zipkin -e MYSQL_HOST=microservices-mysql8 alpertoy/zipkin-server:v1`

Zipkin might take some time to be started.

Zipkin UI
`localhost:9411/zipkin`
